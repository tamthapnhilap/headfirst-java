# Java

## Version

JDK 1.8(lastest)

```bash
PS E:\tamthapnhilap\headfirst-java> java -version
java version "1.8.0_192"
Java(TM) SE Runtime Environment (build 1.8.0_192-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.192-b12, mixed mode)
```

## Run

- https://docs.oracle.com/javase/tutorial/getStarted/cupojava/win32.html

```bash
javac MyApp.java
java -cp . MyApp # java -classpath . MyApp
```

## Design a simple contact app

1.Systems

- Website
- Software components
- Business Process
- App

2.Actors

- Primary actors : initiates the use of system - User
- Secondary actors: Reactionary - Admin

3.Use Cases

Represents an action that accomplishes some sort of task within the system:

- Login
- Find contact from list
- Add more contact on the list
- Edit a contact in the list
- Remove a contact in the list
- Recover a contact that has been removed
- Change Password
- Reset Password

4.Relationships

